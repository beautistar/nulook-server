/*
SQLyog Community Edition- MySQL GUI v8.03 
MySQL - 5.5.50-0ubuntu0.14.04.1 : Database - nulook
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`nulook` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `nulook`;

/*Table structure for table `tb_customer` */

DROP TABLE IF EXISTS `tb_customer`;

CREATE TABLE `tb_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone1` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `phone2` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_customer` */

insert  into `tb_customer`(`id`,`user_id`,`company`,`contact`,`address`,`city`,`phone1`,`phone2`,`email`) values (2,1,'dad','dsfdsa','asdfdsasdfsd','asdfasd','asdf','asdfsa','asdf'),(3,1,'dads','dsfdsaw','asdfdsasdfsdw','asdfasd','asdfw','asdfsa','asdf'),(4,1,'Roy','wryiop','ffghh','Sri','1228','566','too'),(5,1,'Royrgh','wryiop','ffghh','Sri','1228555','566','too'),(6,1,'Royrghty','wryiop','ffghh','Sri','12285552','566','too'),(7,1,'Royrghtyt','wryiop','ffghh','Sri','122855522','566','too'),(8,1,'Royrghtyt','wryiop','ffghh','Sri','1228555225','566','too'),(9,1,'Royrg','wryiop','ffghh','Sri','122855','566','too');

/*Table structure for table `tb_order` */

DROP TABLE IF EXISTS `tb_order`;

CREATE TABLE `tb_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `style` varchar(10) NOT NULL,
  `color` varchar(200) NOT NULL,
  `s` int(10) NOT NULL,
  `m` int(10) NOT NULL,
  `l` int(10) NOT NULL,
  `xl` int(10) NOT NULL,
  `1xl` int(10) NOT NULL,
  `2xl` int(10) NOT NULL,
  `3xl` int(10) NOT NULL,
  `qty` int(10) NOT NULL,
  `price` float(10,2) NOT NULL,
  `disc` int(10) NOT NULL,
  `total` int(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tb_order` */

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `permission` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id`,`username`,`password`,`permission`) values (1,'nulook','123456',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
