<?php

        $setCounter = 0;

        $setExcelName = "Order_".date('Ymd_his', time());

        $setSql = "SELECT * from tb_order";                                                  
        
        $setRec = $this->db->query($setSql);  

        $setCounter = $setRec->num_fields();
        $setMainHeader = "";
        $setData = "";
        
        for ($i = 0; $i < $setCounter; $i++) {

            $setMainHeader .= $setRec->list_fields()[$i]."\t";
        }

        foreach($setRec->result() as $rec)  {
            
          $rowLine = '';
          foreach($rec as $value)       {
            if(!isset($value) || $value == "")  {
              $value = "\t";
            }   else  {
//            It escape all the special charactor, quotes from the data.
              $value = strip_tags(str_replace('"', '""', $value));
              $value = '"' . $value . '"' . "\t";
            }
            $rowLine .= $value;
          }
          $setData .= trim($rowLine)."\n";
        }
          $setData = str_replace("\r", "", $setData);

        if ($setData == "") {
          $setData = "\nno matching records found\n";
        }

        $setCounter = $setRec->num_fields();

//          This Header is used to make data download instead of display the data
        header("Content-type: application/octet-stream");

        header("Content-Disposition: attachment; filename=".$setExcelName."_Reoprt.xls");

        header("Pragma: no-cache");
        header("Expires: 0");

//            It will print all the Table row as Excel file row with selected column name as header.
        echo ucwords($setMainHeader)."\n".$setData."\n";
        ?>
