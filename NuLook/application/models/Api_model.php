<?php

class Api_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function usernameExist($name) {

        $this->db->where('username', $name);         
        return $this->db->get('tb_user')->num_rows();
    }
    
    function phone1Exist($phone) {
        
        $this->db->where('phone1', $phone);         
        return $this->db->get('tb_customer')->num_rows();       
    }
    
    function phone2Exist($phone) {
    
        $this->db->where('phone2', $phone);         
        return $this->db->get('tb_customer')->num_rows();       

    }
    
    function addCustomer($user_id, $company, $contact, $address, $city, $phone1, $phone2, $email) {
        
        $this->db->set('user_id', $user_id);
        $this->db->set('company', $company);
        $this->db->set('contact', $contact);
        $this->db->set('address', $address);
        $this->db->set('city', $city);
        $this->db->set('phone1', $phone1);
        $this->db->set('phone2', $phone2);
        $this->db->set('email', $email);
        
        $this->db->insert('tb_customer');
        $this->db->insert_id();
    }
    
    function getAllCustomer() {
        
        $result = array();         
        $this->db->order_by('company', 'ASC');
        $query = $this->db->get('tb_customer');
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $customer) {
                
                $arr = array('idx' => $customer->id,  
                             'company' => $customer->company,
                             'contact' => $customer->contact,
                             'address' => $customer->address,                               
                             'city' => $customer->city,         
                             'phone1' => $customer->phone1,
                             'phone2' => $customer->phone2,
                             'email' => $customer->email
                             );
                             
                array_push($result, $arr);
            }
        }
        
        return $result;
    }
    
    function getCustomer($user_id) {
        
        $result = array();
        
        $this->db->order_by('company', 'ASC');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('tb_customer');
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $customer) {
                
                $arr = array('idx' => $customer->id,  
                             'company' => $customer->company,
                             'contact' => $customer->contact,
                             'address' => $customer->address,
                             'city' => $customer->city,
                             'phone1' => $customer->phone1,
                             'phone2' => $customer->phone2,
                             'email' => $customer->email
                             );
                             
                array_push($result, $arr);
            }
        }
        
        return $result;
    }
    
    function editCustomer($id, $company, $contact, $address, $city, $phone1, $phone2, $email) {
        
        $this->db->where('id', $id);
        
        $this->db->set('company', $company);
        $this->db->set('contact', $company);
        $this->db->set('address', $company);
        $this->db->set('city', $company);
        $this->db->set('phone1', $company);
        $this->db->set('phone2', $company);
        $this->db->set('email', $company);
        
        $this->db->update('tb_customer');
    }
    
    function deleteCustomer($id) {
        
        $this->db->where('id', $id);
        $this->db->delete('tb_customer');
    }
    
    function getOrders() {
        
        return $this->db->get('tb_order')->result();
    }
}
?>
