<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller  {


    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('api_model');
        $this->load->library('session'); 
        
//        $this->load->library('PHPExcel');
//        $this->load->library('PHPExcel/IOFactory');    
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

     private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
     }

     /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

     private function doRespondSuccess($result){

         $this->doRespond(0, $result);
     }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

     private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
     }
     
     function login($username, $password) { 
         
         $result = array();
         $username = $this->doUrlDecode($username);
         $password = $this->doUrlDecode($password);
                           
         $is_exist = $this->api_model->usernameExist($username);

         if ($is_exist == 0) {
             $this->doRespond(101, $result);  // Unregistered user.
             return;
         }

         $row = $this->db->get_where('tb_user', array('username'=>$username))->row();
         $pwd = $row->password;
         if ($password != $pwd){  // wrong password.

             $this->doRespond(102, $result);
             return; 
         }
         
         $result['idx'] = $row->id;
         $result['username'] = $row->username;
         $result['permission'] = $row->permission;

         $this->doRespondSuccess($result);          
     }
     
     function getCustomer($user_id) {
         
         $result = array();
         
         $result['customer_infos'] = $this->api_model->getCustomer($user_id);
         
         $this->doRespondSuccess($result); 
         
     }
     
      function getAllCustomer() {
     
         $result = array();
         
         $result['customer_infos'] = $this->api_model->getAllCustomer();
         
         $this->doRespondSuccess($result); 
         
     }
     
     function addCustomer() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $company = $this->doUrlDecode($_POST['company']);
         $contact = $this->doUrlDecode($_POST['contact']);
         $address = $this->doUrlDecode($_POST['address']);
         $city = $this->doUrlDecode($_POST['city']);
         $phone1 = $this->doUrlDecode($_POST['phone1']);
         $phone2 = $this->doUrlDecode($_POST['phone2']);
         $email = $this->doUrlDecode($_POST['email']);
         
         // phone number already exist
         if ($this->api_model->phone1Exist($phone1) > 0) {
             
             $this->doRespond(103, $result);
             return;
         }
         
         $this->api_model->addCustomer($user_id, $company, $contact, $address, $city, $phone1, $phone2, $email);
         
         $this->doRespondSuccess($result);            
     }
     
     function editCustomer() {
         
         $result = array();
         
         $id = $this->doUrlDecode($_POST['customer_id']);
         $company = $this->doUrlDecode($_POST['company']);
         $contact = $this->doUrlDecode($_POST['contact']);
         $address = $this->doUrlDecode($_POST['address']);
         $city = $this->doUrlDecode($_POST['city']);
         $phone1 = $this->doUrlDecode($_POST['phone1']);
         $phone2 = $this->doUrlDecode($_POST['phone2']);
         $email = $this->doUrlDecode($_POST['email']);
         
         // phone number already exist
//         if ($this->api_model->phone1Exist($phone1) > 0) {
//             
//             $this->doRespond(103, $result);
//             return;
//         }
         
         $this->api_model->editCustomer($id, $company, $contact, $address, $city, $phone1, $phone2, $email);
         
         $this->doRespondSuccess($result); 
         
     } 
     
     function deleteCustomer($id) {
         
         $result = array();
         
         $this->api_model->deleteCustomer($id);
         
         $this->doRespondSuccess($result);
     }
     
     function submitOrder() {
         
         $request = array();
         $result = array();         

         $data = json_decode(file_get_contents('php://input'), true);

         $request = $data["order_list"];
         
         $note = $this->doUrlDecode($data['note']);
         $customer = $this->doUrlDecode($data['customer']);
         $total = $this->doUrlDecode($data['total']); 
         
         foreach($request as $order) {
             
             $this->db->insert('tb_order', $order);
         }
         ////////////////////////////
                
         $to = "ks_salesagency@hotmail.com";
         
         $subject = "NuLook Fashion Order (".$customer.")";
         $message = "New order was submit<br><br>"
                  . "You can download from here<br><br>"
                  . "http://52.43.164.188/index.php/admin/export";
         
         $from = "Nu-Look fashions";
         $headers = "Mime-Version:1.0\n";
         $headers .= "Content-Type : text/html;charset=UTF-8\n";
         $headers .= "From:" . $from;           
        
         if(mail($to, $subject, $message, $headers)){
             
            $subject = "NuLook Order Submit.";
            $message = "<html><body>";
           
            $number = time();
            /////////////////////////
            $message = '<html><body>';
            $message .= '<div>Nu-Look Fashions</div>';
            $message .= '<div> Order Submit To :'.$to.'</div>';
            $message .= '<br>';
            $message .= '<img src="http://52.43.164.188/fashion.png" alt="NuLook-Fashions" /><br>';
            $message .= "<h3>NO : ".$number."</h3>";
            $message .= '<br>';
            $message .= '<strong>Customer : '.$customer.'</strong>';
            $message .= '<br>';
            $message .= '<br>';
            $message .= '<table rules="all" style="border-color: #000;" cellpadding="10" border = 1>';
            
            $message .= "<tr style='background: #eee;'><td><strong>No</strong></td><td><strong>Style</strong></td><td><strong>Colour</strong></td><td><strong>Description</strong></td><td><strong>S</strong></td><td><strong>M</strong></td><td><strong>L</strong></td><td><strong>XL</strong></td><td><strong>1XL</strong></td><td><strong>2XL</strong></td><td><strong>3XL</strong></td><td><strong>Qty</strong></td><td><strong>Price</strong></td><td><strong>Disc</strong></td><td><strong>Total</strong></td></tr>";
            $cnt = 0;
            foreach ($request as $invoice) {
                
                $cnt ++;     
                $message .= "<tr style='background: #eee;'><td>".$cnt."</td><td>". $invoice['style']."</td><td>".$invoice['color']."</td><td>".$invoice['description']."</td><td>".$invoice['s']."</td><td>".$invoice['m']."</td><td>".$invoice['l']."</td><td>".$invoice['xl']."</td><td>".$invoice['1xl']."</td><td>".$invoice['2xl']."</td><td>".$invoice['3xl']."</td><td>".$invoice['qty']."</td><td>".$invoice['price']."</td><td>".$invoice['disc']."</td><td>".$invoice['total']."</td></tr>";       
            }
            
            $message .= "<tr><td colspan = 13>".$note."</td><td>SubTotal</td><td>".$total."</td></tr>";
            
            $message .= "</table><br>";
            $message .= "<center>Customer 's Signature</center>";
            $message .= "1. Agreement's: All agreement's of sales are made contingent upon delay or failure due to fire, strikes, lockouts, machinery breakdown or 
                        other causes beyond sellers control.<br>";
            $message .= "2. Quotations & Prices: Orders are accepted subject to revision in price and/ ore delivery if necessitated by legislation taxes, 
                        foreign exchange rate or raw material costs.<br>";
            $message .= "3. Shortages and Claims must be made within 14 days after receipt. No returns without prior authorization.<br>";
            $message .= "4. The customer agrees that the seller shall have the right to repossess goods where payment is not made in full on the due date.<br>";
            $message .= "5. Seller will not accept any returns of goods after 14 days.<br>";
            $message .= "6. Substitutions of items can and will only occur if pre-packs are not ordered.<br>";
            $message .=  "<center>Email : contact@nulookfashions.ca  Our policies are on our website.</center>";
            
            $message .= "</body></html>";  
            
            $from = "NuLook";
            $headers = "Mime-Version:1.0\n";
            $headers .= "Content-Type : text/html;charset=UTF-8\n";
            $headers .= "From:" . $from;
            
            if (mail($to, $subject, $message, $headers)) {
            
                $this->doRespondSuccess($result);
            } 
         }
             
     }
     
     
     function sendInvoice($id, $target_email) {
         
         $request = array();
         $result = array();
         
         $data = json_decode(file_get_contents('php://input'), true);

         $request = $data["order_list"];
         
         $note = $this->doUrlDecode($data['note']);
         $customer = $this->doUrlDecode($data['customer']);
         $total = $this->doUrlDecode($data['total']); 
          
        $to = $this->doUrlDecode($target_email);
        
        $subject = "NuLook Order Invoice";
        $message = "<html><body>";
       
        $number = time();
        /////////////////////////
        $message = '<html><body>';
        $message .= '<div>Nu-Look Fashions</div>';
        $message .= '<div> Invoice To :'.$to.'</div>';
        $message .= '<br>';
        $message .= '<img src="http://52.43.164.188/fashion.png" alt="NuLook-Fashions" /><br>';
        $message .= '<br>';
        $message .= "<h3>NO : ".$number."</h3></div>";
        $message .= '<br>';
        $message .= '<strong>Customer : '.$customer.'</strong>';
        $message .= '<br>';
        $message .= '<br>';
        $message .= '<table rules="all" style="border-color: #000;" cellpadding="10" border = 1>';
        
        $message .= "<tr style='background: #eee;'><td><strong>SNo.</strong></td><td><strong>Style</strong></td><td><strong>Colour</strong></td><td><strong>Description</strong></td><td><strong>S</strong></td><td><strong>M</strong></td><td><strong>L</strong></td><td><strong>XL</strong></td><td><strong>1XL</strong></td><td><strong>2XL</strong></td><td><strong>3XL</strong></td><td><strong>Qty</strong></td><td><strong>Price</strong></td><td><strong>Disc</strong></td><td><strong>Total</strong></td></tr>";
        $cnt = 0;
        foreach ($request as $invoice) {
            
            $cnt ++;     
            $message .= "<tr style='background: #eee;'><td>".$cnt."</td><td>". $invoice['style']."</td><td>".$invoice['color']."</td><td>".$invoice['description']."</td><td>".$invoice['s']."</td><td>".$invoice['m']."</td><td>".$invoice['l']."</td><td>".$invoice['xl']."</td><td>".$invoice['1xl']."</td><td>".$invoice['2xl']."</td><td>".$invoice['3xl']."</td><td>".$invoice['qty']."</td><td>".$invoice['price']."</td><td>".$invoice['disc']."</td><td>".$invoice['total']."</td></tr>";       
        }
        $message .= "<tr><td colspan = 13>".$note."</td><td>SubTotal</td><td>".$total."</td></tr>";
        
        $message .= "</table><br>";
        $message .= "<center>Customer 's Signature</center>";
        $message .= "1. Agreement's: All agreement's of sales are made contingent upon delay or failure due to fire, strikes, lockouts, machinery breakdown or 
                    other causes beyond sellers control.<br>";
        $message .= "2. Quotations & Prices: Orders are accepted subject to revision in price and/ ore delivery if necessitated by legislation taxes, 
                    foreign exchange rate or raw material costs.<br>";
        $message .= "3. Shortages and Claims must be made within 14 days after receipt. No returns without prior authorization.<br>";
        $message .= "4. The customer agrees that the seller shall have the right to repossess goods where payment is not made in full on the due date.<br>";
        $message .= "5. Seller will not accept any returns of goods after 14 days.<br>";
        $message .= "6. Substitutions of items can and will only occur if pre-packs are not ordered.<br>";
        $message .=  "<center>Email : contact@nulookfashions.ca  Our policies are on our website.<center>";
        
        $message .= "</body></html>";  
        
        $from = "NuLook";
        $headers = "Mime-Version:1.0\n";
        $headers .= "Content-Type : text/html;charset=UTF-8\n";
        $headers .= "From:" . $from;           
        
        if(mail($to, $subject, $message, $headers) || mail("ks_salesagency@hotmail.com", $subject, $message, $headers)) {             
               
                $this->doRespondSuccess($result);
                        
        }   else {             
            $this->doRespond(105, $result);
        }
     } 
}
?>
